<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/button', function () {
    return view('button');
});

Route::get('/', 'DrinkController@index');

Route::get('/button/{group}/{kind}', 'DrinkController@create');