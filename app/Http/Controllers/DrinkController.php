<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Charts;

class DrinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sINF=DB::table('drinks')->where('kind',0)->where('group',0)->count();
        $bINF=DB::table('drinks')->where('kind',1)->where('group',0)->count();
        $sWIF=DB::table('drinks')->where('kind',0)->where('group',1)->count();
        $bWIF=DB::table('drinks')->where('kind',1)->where('group',1)->count();
        $sSON=DB::table('drinks')->where('kind',0)->where('group',2)->count();
        $bSON=DB::table('drinks')->where('kind',1)->where('group',2)->count();

        //Overview: https://erik.cat/projects/Charts/docs/5

        $sPie=Charts::create('pie', 'google')
            ->title('Schnapskuchen')
            ->labels(['INF', 'WIF', 'Sonstige'])
            ->values([$sINF,$sWIF,$sSON])
            ->dimensions(0,500);

        $bPie=Charts::create('pie', 'google')
            ->title('Bierkuchen')
            ->labels(['INF', 'WIF', 'Sonstige'])
            ->values([$bINF,$bWIF,$bSON])
            ->dimensions(0,500);

        $chart = Charts::multi('bar', 'google')
            ->title('')
            // A dimension of 0 means it will t ake 100% of the space
            ->dimensions(0, 400) // Width x Height
            // This defines a preset of colors already done:)
            ->template("material")
            // You could always set them manually
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('INF', [$bINF,$sINF])
            ->dataset('WIF', [$bWIF,$sWIF])
            ->dataset('Sonstige', [$bSON,$sSON])
            // Setup what the values mean
            ->labels(['Bier', 'Schnaps']);

        return view('welcome', ['chart' => $chart,'sPie'=>$sPie,'bPie'=>$bPie]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($group,$kind)
    {
        DB::table('drinks')->insert(
            ['group' => $group, 'kind' => $kind]
        );
        return redirect('/button');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
