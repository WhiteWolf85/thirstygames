<?php

use Illuminate\Database\Seeder;

class DrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10;$i++) {
            DB::table('drinks')->insert([
                'group' => numberBetween($min = 0, $max = 2),
                'kind' => numberBetween($min = 0, $max = 1),
            ]);
        }
    }
}
