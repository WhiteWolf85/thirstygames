<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .group {
                border: 2px;
                border-style:solid;
                border-color:black;
                margin-bottom:10px;
                padding:5px;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 34px;
            }

            .links > a {
                font: bold 44px Arial;
                text-decoration: none;
                background-color: #EEEEEE;
                color: #333333;
                padding: 2px 6px 2px 6px;
                border-top: 1px solid #CCCCCC;
                border-right: 1px solid #333333;
                border-bottom: 1px solid #333333;
                border-left: 1px solid #CCCCCC;
                margin-bottom:10px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">

                <div class="group">
                    <div class="title m-b-md">
                        Informatik
                    </div>
                    <div class="links">
                        <a href="/button/0/0">Schnaps</a>
                        <a style="margin-left:15px" href="/button/0/1">Bier</a>
                    </div>
                </div>

                <div class="group">
                    <div class="title m-b-md">
                        Wirtschaftsinformatik
                    </div>
                    <div class="links">
                        <a href="/button/1/0">Schnaps</a>
                        <a style="margin-left:15px" href="/button/1/1">Bier</a>
                    </div>
                </div>

                <div class="group">
                    <div class="title m-b-md">
                        Sonstige
                    </div>
                    <div class="links">
                        <a href="/button/2/0">Schnaps</a>
                        <a style="margin-left:15px" href="/button/2/1">Bier</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
